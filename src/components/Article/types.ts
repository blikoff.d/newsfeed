export interface NewsItemDetails {
  id: number
  lang: string
  date: string
  title: string
  description: string
  image: string
  link: string
  author: string
  text: string
  category: {
    id: number
    name: string
  }
  source: {
    id: number
    name: string
    site: string
  }
}

export interface NewsItemRelated {
  category_id: number
  date: Date
  description: string
  id: number
  image: string
  lang: string
  source_id: number
  title: string
}
