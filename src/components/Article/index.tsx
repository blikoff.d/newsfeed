import { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'

import { RelatedSmallArticle } from 'components/RelatedSmallArticle'
import { SingleLineTitleArticle } from 'components/SingleLineTitleArticle'
import { Loader } from 'components/Loader'
import { getApiUrl, getRandomIntFromInterval } from 'utils'

import './Article.css'
import { CategoriesType, NewsItemType, SourcesType } from 'types'
import { NewsItemDetails, NewsItemRelated } from './types'

export const Article = () => {
  const { newsId } = useParams()

  const [selectedNewsItem, setSelectedNewsItem] = useState<NewsItemDetails | null>(null)
  const [relatedNews, setRelatedNews] = useState<Array<NewsItemRelated>>([])
  const [categories, setCategories] = useState<CategoriesType>([])
  const [sources, setSources] = useState<SourcesType>([])
  const [suggestedNews, setSuggestedNews] = useState<Array<NewsItemType>>([])

  useEffect(() => {
    const currentNews = fetch(`${getApiUrl()}news/full/${newsId || ''}`)
    const currentRelatedNews = fetch(`${getApiUrl()}news/related/${newsId || ''}`)
    const currentCategories = fetch(`${getApiUrl()}categories`)
    const currentSources = fetch(`${getApiUrl()}sources`)
    const currentSuggestedNews = fetch(`${getApiUrl()}ru/news/`)

    Promise.all([currentNews, currentRelatedNews, currentCategories, currentSources, currentSuggestedNews])
      .then(([currentNews, currentRelatedNews, currentCategories, currentSources, currentSuggestedNews]) => {
        return Promise.all([
          currentNews.json(),
          currentRelatedNews.json(),
          currentCategories.json(),
          currentSources.json(),
          currentSuggestedNews.json(),
        ])
      })
      .then(
        ([
          currentNewsData,
          currentRelatedNewsData,
          currentCategoriesData,
          currentSourcesData,
          currentSuggestedNews,
        ]) => {
          const randomIdx = getRandomIntFromInterval(0, 17)

          setSelectedNewsItem(currentNewsData)
          setRelatedNews(currentRelatedNewsData.items)
          setCategories(currentCategoriesData)
          setSources(currentSourcesData)
          setSuggestedNews(
            currentSuggestedNews.items.filter((el: unknown, idx: number) => idx >= randomIdx).slice(0, 3),
          )
        },
      )
  }, [newsId])

  return (
    <section className="article-page">
      {selectedNewsItem ? (
        <>
          <article className="article">
            <div className="grid container article__main">
              <div className="article__content">
                <div className="article__title-container">
                  <h1 className="article__title">{selectedNewsItem.title}</h1>

                  <div className="grid">
                    <span className="article-category article__category">Новости</span>
                    <span className="article-date article__date">
                      {new Date(selectedNewsItem.date).toLocaleDateString('ru-RU', {
                        month: 'long',
                        day: 'numeric',
                      })}
                    </span>
                  </div>
                </div>

                <p>{selectedNewsItem.description}</p>
                <p>{selectedNewsItem.text}</p>
                <img src={selectedNewsItem.image} />
              </div>

              <div className="article__small-column">
                {relatedNews &&
                  relatedNews.map((relatedNewsItem) => (
                    <RelatedSmallArticle
                      key={relatedNewsItem.id}
                      categories={categories}
                      sources={sources}
                      {...relatedNewsItem}
                    />
                  ))}
              </div>
            </div>
          </article>

          <section className="article-page__related-articles">
            <div className="container">
              <h2 className="article-page__related-articles-title">Читайте также:</h2>

              <div className="grid article-page__related-articles-list">
                {suggestedNews.map((el) => (
                  <SingleLineTitleArticle categories={categories} sources={sources} key={el.id} {...el} />
                ))}
              </div>
            </div>
          </section>
        </>
      ) : (
        <Loader />
      )}
    </section>
  )
}
