import './MainArticle.css'
import { MainArticleProps } from './types'

export const MainArticle = ({ src, mainNewsItem, category, onClick }: MainArticleProps) => {
  return (
    <article className="main-article" onClick={onClick}>
      <div className="main-article__image-container">
        <img
          className="main-article__image"
          src={
            mainNewsItem.image ||
            'https://images.unsplash.com/photo-1530840197133-665af68f9d71?q=80&w=3548&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D'
          }
          alt="Фото новости"
        />
      </div>
      <div className="main-article__content">
        <span className="article-category main-article__category">{category}</span>
        <h2 className="main-article__title">{mainNewsItem.title}</h2>
        <p className="main-article__text">{mainNewsItem.description}</p>
        <span className="article-source main-article__source">{src}</span>
      </div>
    </article>
  )
}
