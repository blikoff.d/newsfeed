import { NewsItemType } from 'types'

export interface MainArticleProps {
  src?: string
  mainNewsItem: NewsItemType
  category?: string
  onClick: () => void
}
