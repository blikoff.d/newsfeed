export interface AdminCardProps {
  title: string
  image: string
  text: string
}
