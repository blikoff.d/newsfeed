import { useNavigate } from 'react-router-dom'
import './RelatedSmallArticle.css'
import { RelatedSmallArticleProps } from './types'

export const RelatedSmallArticle = ({
  title,
  image,
  categories,
  sources,
  category_id,
  source_id,
  id,
}: RelatedSmallArticleProps) => {
  const navigate = useNavigate()
  const src = sources.find((source) => source.id === source_id)?.name || ''
  const category = categories.find((category) => category.id === category_id)?.name || ''

  return (
    <article className="related-small-article" onClick={() => navigate('/news/' + id)}>
      <img className="related-small-article__image" src={image} />
      <div className="related-small-article__content">
        <span className="article-category related-small-article__category">{category}</span>
        <h2 className="related-small-article__title">{title}</h2>
        <span className="article-source related-small-article__source">{src}</span>
      </div>
    </article>
  )
}
