import { RouterProvider, createBrowserRouter } from 'react-router-dom'
import { Article } from 'components/Article'
import { MainLayout } from 'pages/MainLayout'
import { NewsCategory } from 'pages/NewsCategory'
import { NotFound404 } from 'pages/NotFound404'
import { Admin } from 'pages/Admin'
import { EditArticle } from 'pages/EditArticle'
import { AdminArticles } from 'components/AdminArticles'

const router = createBrowserRouter([
  {
    path: '/',
    element: <MainLayout />,
    children: [
      {
        path: 'main',
        element: <NewsCategory />,
      },
      {
        path: 'tech',
        element: <NewsCategory />,
      },
      {
        path: 'sport',
        element: <NewsCategory />,
      },
      {
        path: 'politics',
        element: <NewsCategory />,
      },
      {
        path: 'fashion',
        element: <NewsCategory />,
      },
      {
        path: '/news/:newsId',
        element: <Article></Article>,
      },
    ],
  },
  {
    path: 'admin/',
    element: <Admin />,
    children: [
      { path: 'articles', element: <AdminArticles /> },
      { path: 'articles/edit/:id', element: <EditArticle /> },
      { path: 'articles/create', element: <EditArticle /> },
    ],
  },

  {
    path: '*',
    element: <NotFound404 />,
  },
])

export function App() {
  return <RouterProvider router={router} />
}
