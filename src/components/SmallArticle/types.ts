import { NewsItemType } from 'types'

export interface SmallArticleProps {
  smallNewsItem: NewsItemType
  src?: string
  onClick: () => void
}
