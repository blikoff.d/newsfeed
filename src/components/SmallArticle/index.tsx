import './SmallArticle.css'
import { SmallArticleProps } from './types'

export const SmallArticle = ({ smallNewsItem, src, onClick }: SmallArticleProps) => {
  return (
    <article className="small-article" onClick={onClick}>
      <h2 className="small-article__title">{smallNewsItem.title}</h2>
      <p className="small-article__caption">
        <span className="article-date small-article__date">
          {new Date(smallNewsItem.date).toLocaleDateString('ru-RU', {
            month: 'long',
            day: 'numeric',
          })}
        </span>
        <span className="article-source small-article__source">{src}</span>
      </p>
    </article>
  )
}
