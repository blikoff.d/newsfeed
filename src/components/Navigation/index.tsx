import { Link, useLocation } from 'react-router-dom'

import { categoriesConfig } from 'utils'
import logo from 'images/logo.svg'

import './Navigation.css'
import { NavigationProps } from './types'

export const Navigation = ({ rootClass }: NavigationProps) => {
  const location = useLocation()

  return (
    <>
      <nav className={`navigation grid ${rootClass}`}>
        <div className="navigation__logo">
          <img src={logo} alt="logo" className="navigation__image" />
        </div>
        <ul className="navigation__list">
          {Object.entries(categoriesConfig)
            .filter(([, value]) => value.isVisible)
            .map(([key, value]) => {
              return (
                <li key={key} className="navigation__item">
                  <Link
                    to={`/${key}`}
                    className={`navigation__link ${location.pathname === `/${key}` && 'navigation__link-active'} ${location.pathname === '/' && key === 'main' && 'navigation__link-active'} `}
                    data-href={key}
                  >
                    {value.textName}
                  </Link>
                </li>
              )
            })}
        </ul>
      </nav>
    </>
  )
}
