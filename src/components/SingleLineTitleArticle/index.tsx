import { useNavigate } from 'react-router-dom'
import './SingleLineTitleArticle.css'
import { SingleLineTitleArticleProps } from './types'

export const SingleLineTitleArticle = ({
  image,
  title,
  id,
  source_id,
  category_id,
  sources,
  categories,
  description,
}: SingleLineTitleArticleProps) => {
  const navigate = useNavigate()
  const src = sources.find((source) => source.id === source_id)?.name || ''
  const category = categories.find((category) => category.id === category_id)?.name || ''

  return (
    <article className="single-line-title-article" onClick={() => navigate('/news/' + id)}>
      <img className="single-line-title-article__image" src={image} alt="Фото новости" />
      <span className="article-category single-line-title-article__category">{category}</span>
      <h2 className="single-line-title-article__title">{title}</h2>
      <p className="single-line-title-article__text">{description}</p>
      <span className="article-source single-line-title-article__source">{src}</span>
    </article>
  )
}
