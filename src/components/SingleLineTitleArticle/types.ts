import { CategoriesType, SourcesType } from 'types'

export interface SingleLineTitleArticleProps {
  title: string
  image: string
  sources: SourcesType
  categories: CategoriesType
  category_id: number
  source_id: number
  id: number
  description: string
}
