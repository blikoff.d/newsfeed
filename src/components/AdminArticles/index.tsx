import { useNavigate } from 'react-router-dom'
import { Box, Button, Grid, Typography } from '@mui/material'
import ControlPointIcon from '@mui/icons-material/ControlPoint'
import { AdminCard } from 'components/AdminCard'

const partnerNews = [
  { title: 'Партнерские статьи 1', image: 'https://placehold.co/600x400', text: 'Партнерские статьи 1', id: 1 },
  { title: 'Партнерские статьи 2', image: 'https://placehold.co/600x400', text: 'Партнерские статьи 1', id: 2 },
  { title: 'Партнерские статьи 3', image: 'https://placehold.co/600x400', text: 'Партнерские статьи 1', id: 3 },
  { title: 'Партнерские статьи 4', image: 'https://placehold.co/600x400', text: 'Партнерские статьи 1', id: 4 },
]

export const AdminArticles = () => {
  const navigate = useNavigate()

  return (
    <>
      <Box sx={{ display: 'flex', mb: 3, justifyContent: 'space-between' }}>
        <Typography variant="h4">Партнерские статьи</Typography>

        <Button variant="contained" color="success" onClick={() => navigate('/admin/articles/create')}>
          <ControlPointIcon />
        </Button>
      </Box>

      <Grid container spacing={2}>
        {partnerNews.map(({ title, text, image, id }) => (
          <Grid item xs={4} key={id} onClick={() => navigate(`/admin/articles/edit/${id}`)}>
            <AdminCard title={title} text={text} image={image} />
          </Grid>
        ))}
      </Grid>
    </>
  )
}
