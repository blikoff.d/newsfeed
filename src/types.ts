export type SourcesType = Array<{ id: number; name: string }>
export type CategoriesType = Array<{ id: number; name: string }>

export type NewsData = {
  sources: SourcesType
  categories: CategoriesType
  items: Array<NewsItemType>
}

export interface NewsItemType {
  id: number
  lang: string
  date: Date
  title: string
  description: string
  image: string
  source_id: number
  category_id: number
}
