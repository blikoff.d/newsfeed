export type CategoriesType = keyof typeof categoriesConfig

export const categoriesConfig = {
  main: { id: '', textName: 'Главная', isVisible: true },
  tech: { id: '1', textName: 'Технологии', isVisible: true },
  sport: { id: '2', textName: 'Спорт', isVisible: true },
  fashion: { id: '3', textName: 'Мода', isVisible: true },
  politics: { id: '4', textName: 'Политика', isVisible: true },
  other: { id: '5', textName: 'Другое', isVisible: false },
}

export const getApiUrl = () => {
  switch (process.env.NODE_ENV) {
    case 'development':
      return 'http://frontend.karpovcourses.net/api/v2/'
    case 'production':
      return 'https://frontend.karpovcourses.net/api/v2/'
    default:
      throw new Error('Unhandled base url by NODE_ENV value')
  }
}

export const getRandomIntFromInterval = (min: number, max: number) => {
  // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min)
}
