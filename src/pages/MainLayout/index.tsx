import { useEffect } from 'react'
import { Outlet, useLocation, useNavigate } from 'react-router-dom'
import { Navigation } from 'components/Navigation'

import './MainLayout.css'

export function MainLayout() {
  const location = useLocation()
  const navigate = useNavigate()

  useEffect(() => {
    if (location.pathname === '/') {
      navigate('/main')
    }
  }, [location])

  return (
    <>
      <header className="header">
        <div className="container">
          <Navigation rootClass="header__navigation" />
        </div>
      </header>
      <Outlet />
      <footer className="footer">
        <div className="container">
          <Navigation rootClass="footer__navigation" />

          <div className="footer__row">
            <p className="footer__text">
              Сделано на Frontend курсе в
              <a href="https://karpov.courses/frontend" target="_blank" className="footer__link">
                Karpov.Courses
              </a>
            </p>
            <p className="footer__copyright">© 2021</p>
          </div>
        </div>
      </footer>
    </>
  )
}
