import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import notFound1 from 'images/404-1.svg'
import notFound2 from 'images/404-2.svg'
import notFound3 from 'images/404-3.svg'
import notFound4 from 'images/404-4.svg'
import notFound5 from 'images/404-5.svg'
import notFound6 from 'images/404-6.svg'

import './NotFound.css'

const dict = {
  1: notFound1,
  2: notFound2,
  3: notFound3,
  4: notFound4,
  5: notFound5,
  6: notFound6,
}

export const NotFound404 = () => {
  const navigate = useNavigate()

  const [src, setSrc] = useState({ idx: 1, src: notFound1 })

  useEffect(() => {
    const interval = setInterval(() => {
      setSrc((prev) => {
        if (prev.idx >= 6) {
          return { idx: 1, src: notFound1 }
        }

        return { idx: prev.idx + 1, src: dict[`${prev.idx + 1}` as unknown as keyof typeof dict] }
      })
    }, 200)

    return () => clearInterval(interval)
  }, [])

  return (
    <div className="notFoundPage">
      <h1 className="notFoundPage__title">Whoops!</h1>
      <p className="notFoundPage__text">Sorry, this page is not available or broken.</p>

      <img className="notFoundPage__img" src={src.src} alt="404" />

      <button className="btn" style={{ marginTop: '60px' }} onClick={() => navigate('/main')}>
        Back to Home
      </button>
    </div>
  )
}
