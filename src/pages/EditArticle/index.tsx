import { ChangeEvent, FormEvent, useRef, useState } from 'react'
import { useParams } from 'react-router-dom'
import {
  Box,
  Button,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Grid,
  Menu,
  MenuItem,
  TextField,
  Typography,
} from '@mui/material'

import MoreVertIcon from '@mui/icons-material/MoreVert'
import { InputErrors, InputName, InputRefs, InputValues } from './types'
import { getErrors, getImage } from './helpers'

export const EditArticle = () => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null)
  const open = Boolean(anchorEl)
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget)
  }
  const handleClose = () => {
    setAnchorEl(null)
  }

  const { id } = useParams()

  const inputRefs: InputRefs = {
    'company-name': useRef<HTMLInputElement>(),
    title: useRef<HTMLInputElement>(),
    description: useRef<HTMLTextAreaElement>(),
    text: useRef<HTMLTextAreaElement>(),
    image: useRef<HTMLInputElement>(),
  }
  const [inputFile, setInputFile] = useState<File | null>(null)
  const [inputErrors, setInputErrors] = useState<InputErrors>({
    'company-name': '',
    title: '',
    description: '',
    text: '',
    image: '',
  })
  const [inputValues, setInputValues] = useState<InputValues>({
    'company-name': '',
    title: '',
    description: '',
    text: '',
    image: '',
  })
  const onChangeInput = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const input = event.currentTarget
    const name = input.name
    const value = input.value

    setInputValues({
      ...inputValues,
      [name]: value,
    })
  }
  const onSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault()

    // 1. Собрать данные
    const data = new FormData()

    Object.entries(inputValues).forEach(([name, value]) => {
      if (name === 'image') {
        data.append(name, inputFile || new File([], ''))
      } else {
        data.append(name, value)
      }
    })

    // 2. Проверить данные
    const errors = await getErrors(Array.from(data?.entries()) as [InputName, FormDataEntryValue][])
    const errorsEntries = Object.entries(errors)

    // 3.1 Подстветить ошибки
    setInputErrors(errors)

    // 3.2 Сфокусироваться на первом ошибочном инпуте
    const errorInput = errorsEntries.find(([, value]) => value.length > 0)

    if (errorInput) {
      const name = errorInput[0] as InputName
      const inputRef = inputRefs[name]

      if (inputRef.current) {
        inputRef.current.focus()
      }

      return
    }

    // 4. Если все ок, отправить данные
    fetch('https://httpbin.org/post', {
      method: 'POST',
      body: data,
    })
  }

  const showFile = (event: ChangeEvent<HTMLInputElement>) => {
    const files = event.currentTarget.files

    if (files === null || !files.length) {
      return
    }

    const file = files[0]

    if (file.size === 0 || !file.type.startsWith('image/')) {
      return
    }

    setInputFile(file)

    getImage(file).then((image) => {
      setInputValues({
        ...inputValues,
        image: image.src,
      })
    })
  }

  return (
    <Box component="form" noValidate onSubmit={onSubmit}>
      <Grid container spacing={2}>
        <Grid item xs={9}>
          <Typography gutterBottom variant="h4" component="div">
            {id ? `Редактирование статьи по ${id}` : 'Создание статьи'}
          </Typography>
        </Grid>
        <Grid item xs={3}>
          <Box sx={{ display: 'flex', justifyContent: 'flex-end', gap: '8px' }}>
            <Button type="submit" variant="contained" color="success">
              Сохранить
            </Button>

            <div>
              <Button
                aria-controls={open ? 'basic-menu' : undefined}
                aria-haspopup="true"
                aria-expanded={open ? 'true' : undefined}
                onClick={handleClick}
              >
                <MoreVertIcon />
              </Button>
              <Menu anchorEl={anchorEl} open={open} onClose={handleClose}>
                <MenuItem onClick={handleClose}>Удалить статью</MenuItem>
              </Menu>
            </div>
          </Box>
        </Grid>
      </Grid>

      <Grid container spacing={2}>
        <Grid item xs={7}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                fullWidth
                name="company-name"
                label="Компания"
                variant="outlined"
                value={inputValues['company-name']}
                onChange={onChangeInput}
                ref={inputRefs['company-name']}
                error={Boolean(inputErrors['company-name'].length)}
                helperText={inputErrors['company-name']}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                fullWidth
                name="title"
                label="Название статьи"
                variant="outlined"
                onChange={onChangeInput}
                value={inputValues['title']}
                ref={inputRefs['title']}
                error={Boolean(inputErrors['title'].length)}
                helperText={inputErrors['title']}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                fullWidth
                multiline
                maxRows={4}
                name="description"
                label="Подводка"
                variant="outlined"
                value={inputValues['description']}
                onChange={onChangeInput}
                ref={inputRefs['description']}
                error={Boolean(inputErrors['description'].length)}
                helperText={inputErrors['description']}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                name="text"
                fullWidth
                multiline
                maxRows={4}
                label="Текст статьи"
                variant="outlined"
                onChange={onChangeInput}
                value={inputValues['text']}
                ref={inputRefs['text']}
                error={Boolean(inputErrors['text'].length)}
                helperText={inputErrors['text']}
              />
            </Grid>
          </Grid>
        </Grid>

        <Grid item xs={5}>
          <Card sx={{ maxWidth: 345 }}>
            <CardActionArea>
              <CardMedia component="img" height="120" image={inputValues['image']} />
              <CardContent>
                <TextField
                  type="file"
                  name="image"
                  fullWidth
                  variant="outlined"
                  onChange={showFile}
                  ref={inputRefs['image']}
                  error={Boolean(inputErrors['image'].length)}
                  helperText={inputErrors['image']}
                />
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>
      </Grid>
    </Box>
  )
}
