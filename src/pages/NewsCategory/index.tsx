import { useState, useEffect } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'
import { SmallArticle } from 'components/SmallArticle'
import { MainArticle } from 'components/MainArticle'
import { Loader } from 'components/Loader'
import { CategoriesType, categoriesConfig, getApiUrl } from 'utils'

import './NewsCategory.css'
import { NewsData } from 'types'

export function NewsCategory() {
  const navigate = useNavigate()
  const location = useLocation()
  const [selectedCategory, setSelectedCategory] = useState<CategoriesType>('main')
  const [data, setData] = useState<NewsData>({
    sources: [],
    categories: [],
    items: [],
  })

  const allCategories = Object.keys(categoriesConfig)

  useEffect(() => {
    const category = location.pathname.split('/')[1]
    if (allCategories.includes(category)) {
      setSelectedCategory(category as CategoriesType)
    }
  }, [location.pathname])

  useEffect(() => {
    fetch(`${getApiUrl()}ru/news/${categoriesConfig[selectedCategory].id || ''}`)
      .then((res) => res.json())
      .then((data) => setData(data))
  }, [selectedCategory])

  return (
    <main className="main">
      {data.items.length ? (
        <section className="articles">
          <div className="container grid">
            <section className="articles__big-column">
              {data.items
                .slice(0, 3)
                ?.map((mainNewsItem) => (
                  <MainArticle
                    key={mainNewsItem.title}
                    onClick={() => navigate(`/news/${mainNewsItem.id}`)}
                    category={
                      data.categories.find((dataCategory) => dataCategory.id === mainNewsItem.category_id)?.name
                    }
                    src={data.sources.find((dataSrc) => dataSrc.id === mainNewsItem.source_id)?.name}
                    mainNewsItem={mainNewsItem}
                  />
                ))}
            </section>

            <section className="articles__small-column">
              {data.items.slice(3, 12).map((smallNewsItem) => (
                <SmallArticle
                  key={smallNewsItem.title}
                  onClick={() => navigate(`/news/${smallNewsItem.id}`)}
                  smallNewsItem={smallNewsItem}
                  src={data.sources.find((dataSrc) => dataSrc.id === smallNewsItem.source_id)?.name}
                />
              ))}
            </section>
          </div>
        </section>
      ) : (
        <Loader />
      )}
    </main>
  )
}
